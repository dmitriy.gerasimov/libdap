#ifndef __DAP_RAND_H__
#define __DAP_RAND_H__

// Generate random bytes and output the result to random_array
int randombytes(unsigned char* random_array, unsigned int nbytes);

#endif
